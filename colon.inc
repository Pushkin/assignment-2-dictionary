%define NEXT_LINK 0

%macro colon 2

%2:
    dq NEXT_LINK
    db %1, 0 
    %define NEXT_LINK %2
%endmacro