section .data
%define POINTER_SIZE 8 
%define NOT_FOUND 0


section .text
global find_word
extern string_equals

find_word:
    .loop:
        push rdi
        push rsi
        add rsi, POINTER_SIZE
        call string_equals
        pop rsi 
        pop rdi 
        test rax, rax
        jnz .success
        mov rsi, [rsi]
        test rsi, rsi
        jnz .loop
        mov rax, NOT_FOUND
        ret
    .success:
        mov rax, rsi
        ret