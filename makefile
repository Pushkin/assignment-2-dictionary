ASM=nasm
ASMFLAGS=-f elf64

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

execute: main.o dict.o lib.o
	ld -o $@ $^

.PHONY: clean
clean:
	rm *.o execute