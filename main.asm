section     .data

%include    "words.inc"
%include    "main_lib.inc"
%define     DQ_SIZE 8
%define     BUFFER_SIZE 255





section     .rodata
fatal_reading_msg:  db  "Reading error", 0
fatal_key_msg:      db  "Not found", 0


section     .bss
buffer:     resb    BUFFER_SIZE


section     .text
global      _start


_start:
    mov rdi, buffer                          
    mov rsi, BUFFER_SIZE                        
    call read_word                              
    test rax, rax                           
    jz fatal_reading                          
    push rdx                                     
    mov rdi, rax                                
    mov rsi, NEXT_LINK                            
    call find_word                             
    pop rdx                                     
    test rax, rax                               
    jz fatal_key  



success:
    lea rdi, [(rax) + (1 * DQ_SIZE) + (rdx + 1)]  
    jmp log                                     



fatal_key:
    mov rdi, fatal_key_msg
    jmp err                                     



fatal_reading:
    mov rdi, fatal_reading_msg



log:
    call print_string
    call print_newline
    jmp  exit
